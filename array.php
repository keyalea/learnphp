<!DOCTYPE HTML>
<html lang="en">
<head><meta charset="UTF-8">
<title>Arrays - This seems to be working!</title>
</head>
<body>
<?php
# Why aren't the comments being ignored? They can't start a doc
$days = array( 'Monday', 'Tuesday', 'Wednesday', 'Thursday');
foreach( $days as $value ) {
	echo "&bull; $value " ;
	}
$months = array( 'jan' => 'January',
	'feb' => 'February',
	'mar' => 'March' );
echo '<dl>' ;
foreach( $months as $key => $value ) {
	echo "<dt>$key<dd>$value" ;
	}
echo '</dl>';
$five = range(1,5); 
echo "$five"; # this is not printing not sure why
?>
</body>
</html>
