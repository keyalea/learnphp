<!DOCTYPE HTML>
<html lang="en">
<head><meta charset="UTF-8">
<title>Hey! This seems to be working!</title>
</head>
<body>
<?php
# Why aren't the comments being ignored? They can't start a doc
$phrase = 'The truth is rarely pure' ;
$author = 'Oscar Wilde' ;
echo $phrase ;
echo "<p>It is often said that <q>$phrase</q> </p>" ;
$phrase = $phrase . ' and never simple' ;
echo "<p><q>$phrase</q><cite>- $author </cite></p>" ;
?>
</body>
</html>
